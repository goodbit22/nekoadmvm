#!/usr/bin/bash
# shellcheck source=/dev/null  

# to samo co wyzej dla opcji 5


if [ -f install.sh ]; then
	source install.sh 
else
	echo -e "${RED}Nie ma wymaganego pliku install.sh${WHITE}"
	exit 1;
fi

if [ -f  vagrant_box.sh ]; then
	source vagrant_box.sh
else
	echo -e "${RED}Nie ma wymaganego pliku vagrant_box.sh${WHITE}"
	exit 1;
fi

main(){
				local opcje=("Display virtual machines" "Create virtual machine" "Delete virtual machine" 
				"Modify virtual machine"  "Create Vagrant Box"				
	)
	choose=$(dialog --clear --stdout --title "Main Menu" --backtitle "NekoAdmVm" --menu "Your choice:" 15 40 5 \
					1 "${opcje[0]}" 2 "${opcje[1]}"  3 "${opcje[2]}" 4 "${opcje[3]}" 5 "${opcje[4]}")

	if [ "$choose" == 1 ];then
		list_machines
	elif [ "$choose" == 2 ];then
		select_virtual_machines
	elif [ "$choose" == 3 ];then 
		delete_machine
	elif [ "$choose" == 4 ];then 
		modify_machine
	elif [ "$choose" == 5 ];then 
		create_box_vagrant
	fi
}

list_machines(){
				vm_list=$(VBoxManage list vms)
				dialog --title "List Virtual Machines" --msgbox "$vm_list" 20 120 
				main
}

select_virtual_machines(){
	local options=("Create virtual machine" "Create multiple virtual machines" )
	choose=$(dialog --clear --stdout --title "Window Menu" --backtitle "NekoAdmVm" --menu "Your choice:" 10 50 2 \
						1 "${options[0]}" 2 "${options[1]}"  )
	if [ "$choose" == 1 ];then
		create_machine
	elif [ "$choose" == 2 ];then
		create_many_machines
	else
			main
	fi
}

create_many_machines(){
	status=0
	while [[  "$status" -eq 0 ]]
			do
					dialog --clear --title "Create multiple Machines" --backtitle "NekoAdmVm"  \
					--yesno "Do you want to create a virtual machine ?" 10 30
					status="$?"
					case "$status" in
							0)
								create_machine
								;;
							1)
								main
								;;
					esac
				
		done
}

create_machine(){
	local name_machine=""
	while [[ "$name_machine" == ""  ]] || [[ "$status" -eq 0 ]] 
	do
			name_machine=$(dialog  --title "Name Machine" --stdout --inputbox "Enter new name for the virtual machine" 8 40 )
			check_exists_virtual_machines "$name_machine"
			local status="$?"
			if [[ "$name_machine" == "" ]];then 
				dialog --title "Message" --msgbox "Empty field Name Machine" 5 30 
			elif [ "$status"  -eq 0 ]; then
				dialog --title "Error" --msgbox "The $name_machine virtual machine is exists" 8  50			
			fi
	done
	mapfile -t os < <(vboxmanage list ostypes | grep -w ^ID: | awk '{print $2}')
	local ostype2=""
	w=1
	while [[ "$ostype2" == "" ]] 
	do
		ostype2=$(dialog --clear --stdout --title "Window OS choose" --backtitle "NekoAdmVm" \
  	--radiolist "Select OS:" 20 40 10 \
		$(for i in "${os[@]}"; do  echo "$i $i \\";done))
		if [[ "$ostype2" == "" ]];then 
				dialog --title "Message" --msgbox "you have not selected any operating system" 6 50 
		fi
	done
	local memory2=""
	while [[ "$memory2" == "" ]] 
	do
		memory2=$(dialog --stdout --title "Memory machine"\
		--inputbox "Enter memory" 8 40 	)
		if [[ "$memory2" == "" ]];then 
				dialog --title "Message" --msgbox "empty field memory machine" 8 50 
		fi
	done
	
	local network_nic=("none" "null" "nat" "natnetwork" "bridged" "intnet" "hostonly" "generic")
	local net_nic=""
	while [[ "$net_nic" == "" ]] 
	do
		net_nic=$(dialog --clear --stdout --title "Network Interface" --backtitle "NekoAdmVm" \
		--radiolist "Select virtual card" 15 40 8 \
		$(for s in "${network_nic[@]}"; do echo "$w  $s \\"; w=$((w+1)); done  ))
		if [[ "$net_nic" == "" ]];then 
				dialog --title "Message" --msgbox "You have not selected any network interface" 6 50
				w=1
		fi
	done
	local size2=""
	while [[ "$size2" == "" ]] 
	do
		size2=$(dialog --clear --stdout --title "Create 'dynamic' disk" --backtitle "NekoAdmVm" \
		--inputbox "Enter size disk" 20 40 )
		if [[ "$size2" == "" ]];then 
				dialog --title "Message" --msgbox "empty field disk size" 8 50 	
		fi
	done

	VBoxManage createvm --name "$name_machine" --ostype "$ostype2"	 --basefolder ~   --register
	VBoxManage modifyvm "$name_machine" --memory "$memory2" --vram 18
	VBoxManage modifyvm "$name_machine" --nic1 "${network_nic[$net_nic-1]}" 
	VBoxManage createvm --name "$name_machine" --ostype "$ostype2"	 --basefolder ~   --register	
	VBoxManage createhd --filename ~/"$name_machine".vdi --size "$size2" --format VDI
	VBoxManage storagectl "$name_machine" --name "SATA Controller" --add sata  --controller "IntelAhci"
	VBoxManage storageattach "$name_machine" --storagectl "SATA Controller" --port 0 --device 0 --type hdd \
					--medium ~/"$name_machine".vdi
	local file_iso=""
	local file=""
	while [[ "$file" != *.iso ]] 
	do
		file_iso=$(dialog --stdout --title "Please choose a file iso" --fselect ~ 14 48)
		file="${file_iso##*/}"
		if [[ "$file" != *.iso ]] ;then 
			dialog --title "Message"  --msgbox "File extension is not valid" 10 20 
		fi
	done
	VBoxManage storageattach "$name_machine" --storagectl "SATA Controller" \
					--port 1 --device 0 --type dvddrive --medium "$file_iso" 
	main
}

choose_machine(){
	mapfile -t os_w < <(vboxmanage list vms | awk '{print $1}'| sed 's/"//g') 	
	cancel="$1"
	local window_name="$2"
	name_machine="$3"
	while [[ "$name_machine" == "" ]] 
		do
				COUNTER=1
				RADIOLIST="" 
				for  i in "${os_w[@]}"; do
    			RADIOLIST="$RADIOLIST $COUNTER $i off "
    			COUNTER=$((COUNTER+1))
				done
n
				name_machine=$(dialog --title "$window_name" --stdout --backtitle "NekoAdmVm" \
--checklist "radiolist" 0 0 $COUNTER \
$RADIOLIST)
					if [ "$?" -eq 1 ]; then
						cancel=1
						break
					fi
					if [[ "$name_machine" == "" ]];then 
						dialog --title "Message" --msgbox "Empty field" 5 20 
					fi
		done
}


delete_machine(){
	mapfile -t os_w < <(vboxmanage list vms | awk '{print $1}'| sed 's/"//g') 	
	local cancel=0
	local name_machine=""
	choose_machine "$cancel" "Delete machine" "$name_machine"
	if [ "$cancel" -eq 1 ];then
		main
	else				
	dialog --title "Delete machine" \
	--backtitle "NekoAdmVm" --yesno "Are you sure you want to permanently delete machine ${os_w["$name_machine"-1]}"  7 60
	response=$?
	case $response in
		0) 
			VBoxManage unregistervm --delete "${os_w[$name_machine-1]}"
			dialog --title "Message" --msgbox "Machine ${os_w[$name_machine-1]} has been removed" 8 60 
			main
			;;

		1) main;;
	esac
	fi
}

check_exists_virtual_machines(){                                                                              
     mapfile -t list_virtual_machines < <(VBoxManage list vms | awk '{print $1}' | sed 's/"//g')                 
     local name_machine="$1"                                                                                     
     for vm in "${list_virtual_machines[@]}"                                                                     
     do                                                                                                          
    	if [ "$name_machine" == "$vm" ]; then                                                               			
           return 0                                                                                          
           break                                                                                             
      fi                                                                                                  
    done                                                                                                        
    return 1                                                                                                    
} 


modify_machine(){
	mapfile -t os_w < <(vboxmanage list vms | awk '{print $1}'| sed 's/"//g') 	
	local cancel=0
	local name_machine=""
	choose_machine "$cancel" "Modify virtual machine" "$name_machine"
	if [ "$cancel" -eq 1 ];then
		main
	else				
	local options=("Rename virtual machine" "Resize ram"
		"Change network interface" )
	choose=$(dialog --clear --stdout --title "Modify virtual machine" --backtitle "NekoAdmVm" \
					--menu "Your choice:" 10 60 3 \
					1 "${options[0]}" 2 "${options[1]}" 3 "${options[2]}" )
	
	case "$choose" in 
					1)
						local new_name_machine=""
						while [[ "$new_name_machine" == "" ]] || [[ "$status" -eq 0 ]] 
						do
							new_name_machine=$(dialog  --title "New Name Virtual Machine" --stdout \
											--inputbox "Enter new  name machine" 8 40 )
							check_exists_virtual_machines "$new_name_machine"
							local status="$?"
							if [[ "$new_name_machine" == "" ]];then 
								dialog --title "Message" --msgbox "Empty field New Name Virtual Machine" 5 30 
							elif [ "$status"  -eq 0 ]; then
								dialog --title "Error" --msgbox "Some virtual machine has this name " 8  50  
							fi
						done
						 VBoxManage modifyvm "${os_w[$name_machine-1]}" --name "$new_name_machine"
						 main

						;;
					2)
						local new_size_memory=""
						while [[ "$new_size_memory" == "" ]] 
						do
							new_size_memory=$(dialog  --title  "Size Virtual Machine" --stdout \
											--inputbox "Enter size virtual machine" 8 40 )
							if [[ "$new_size_memory" == "" ]];then 
								dialog --title "Message" --msgbox "Empty field Size Virtual Machine" 5 30 
							fi
						done
						 VBoxManage modifyvm "${os_w[$name_machine-1]}" --memory "$new_size_memory"
						 main			
						 ;;
					3)
						local net_nic=""
						local network_nic=("none" "null" "nat" "natnetwork" "bridged" "intnet" "hostonly" "generic")
						while [[ "$net_nic" == "" ]] 
						do
							net_nic=$(dialog --clear --stdout --title "Network Interface" --backtitle "NekoAdmVm" \
							--radiolist "Select virtual card" 15 40 8 \
							$(for s in "${network_nic[@]}"; do echo "$w  $s \\"; w=$((w+1)); done  ))
							if [[ "$net_nic" == "" ]];then 
								dialog --title "Message" --msgbox "You have not selected any network interface" 6 50
								w=1
							fi
						done
						VBoxManage modifyvm "${os_w[$name_machine-1]}"  --nic1 "${network_nic[$net_nic-1]}"  
						main
						;;
					*):
						main
					;;
				esac
			fi
}

WHITE="\e[37m"
install
main
