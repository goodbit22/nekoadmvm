#!/usr/bin/bash

create_box_vagrant(){
	current_path="$PWD"
	mapfile -t os_w < <(vboxmanage list vms | awk '{print $1}'| sed 's/"//g') 	
	local name_machine=""
	local cancel=0
	choose_machine "$cancel" "Packer Vagrant" "$name_machine"
	if [ "$cancel" -eq 1 ];then
		main
	else	
	dialog --title "Create box Vagrant" --backtitle "NekoAdmVm" \
	--yesno "Are you sure you want to create box Vagrant ${os_w["$name_machine"-1]}"  7 60
	response=$?
	case $response in 
		0)
			path_box="$HOME/vagrant_box"
			if [ -d  "$path_box" ]; then
					true
			else
					mkdir -p "$path_box"
			fi
			cd "$path_box" || return 1
			vagrant package --base "${os_w[$name_machine-1]}"  --output "${os_w[$name_machine-1]}.box" 
			dialog --title "Message" --msgbox "Machine ${os_w[$name_machine-1]} has been created box Vagrant" 8 60 
			sha512sum "${os_w[$name_machine-1]}.box" > "sha512sum_${os_w[$name_machine-1]}.txt"
			cd "$current_path" || return 1
			main
			;;

		1) main;;
	esac
	fi
}
