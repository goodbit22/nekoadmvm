# NekoAdmVM

The script enabling administration of Virtualbox virtual machines through gui dialog

![graphic1](./img/1.png)
![graphic2](./img/2.png)

## Table of Contents

- [NekoAdmVM](#nekoadmvm)
  - [Table of Contents](#table-of-contents)
  - [Features](#features)
  - [Running](#running)
  - [Technologies](#technologies)
  - [Authors](#authors)
  - [License](#license)

## Features

- Create virtual machines
- Delete virtual machines
- Modify virtual machines
- Display virtual machines

## Running

```sh
 bash nekoadmvm.sh
```

## Technologies

Project is created with:

- bash (5.1.8)
- dialog (1.3-20201126)
- awk
- sed

## Authors

***
__goodbit22__ --> 'https://gitlab.com/users/goodbit22'
***

## License

   Apache License Version 2.0